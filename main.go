package main

import (
	"bufio"
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path"
	"reflect"
	"strconv"
	"strings"

	"github.com/charmbracelet/glamour"
	anthropic "github.com/liushuangls/go-anthropic"
	openai "github.com/sashabaranov/go-openai"
	"github.com/spf13/viper"
)

type (
	Parameters struct {
		LoadPrompt    *string
		Prompt        *string
		EditPrompt    *string
		Query         *string
		SelectedModel *Model
		Model         *string
		Session       *string
		MaxTokens     *int
		ShowPrompt    *bool
		LsPrompt      *bool
		ShowQuery     *bool
		Interactive   *bool
		DryRun        *bool
		Stream        *bool
		Raw           *bool
		Price         *bool
		LastSession   *bool
	}
	Model struct {
		Name     string
		Provider *Provider
	}
	Provider struct {
		Name               string
		ApiKeyVariableName string
	}
)

// Read config file $HOME/.config/cbg/config.yaml
func readConf() error {
	home, err := os.UserHomeDir()
	if err != nil {
		log.Fatal("error getting home directory")
	}

	confDir = path.Join(home, CONFIG_CBG)
	sessionsDir = path.Join(confDir, sessionsDir)
	promptsDir = path.Join(confDir, promptsDir)

	dirs := []string{confDir, sessionsDir, promptsDir}
	for _, dir := range dirs {
		if _, err := os.Stat(dir); os.IsNotExist(err) {
			err := os.Mkdir(dir, 0o700)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	viper.AddConfigPath(confDir)

	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	err = viper.ReadInConfig()
	if err == nil {
		return err
	}
	// config file does not exist
	err = viper.SafeWriteConfig()
	if err != nil {
		return err
	}

	return nil
}

func printOut(raw bool, in string) {
	if raw { // raw output
		fmt.Println(in)

		return
	}

	gstyle := "dark" // default style
	cstyle := viper.GetString("style")

	if len(os.Getenv("GLAMOUR_STYLE")) != 0 {
		gstyle = os.Getenv("GLAMOUR_STYLE")
	} else if len(cstyle) > 0 {
		gstyle = cstyle
	}

	out, err := glamour.Render(in, gstyle)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Print(out)
}

func setModel(model string) *Model {
	for _, value := range MODELS {
		if value.Name == model {
			return value
		}
	}

	return &Model{Name: model, Provider: OPENAI_PROVIDER}
}

func loadPrompt(lp string) *string {
	p := ""

	if len(lp) > 0 {
		conf := path.Join(promptsDir, lp)

		b, err := os.ReadFile(conf)
		if err != nil {
			log.Fatalf("error reading %s", conf)
		}

		p = strings.TrimSuffix(string(b), "\n")
	}

	return &p
}

func GenerateCompletion(params Parameters, sess *[]Session, step int) {
	apiKey := getApiKey(*params.SelectedModel.Provider)
	*sess = append(*sess, Session{System: *params.Prompt, User: *params.Query})

	content := ""

	if params.SelectedModel.Provider == ANTHROPIC {
		client := anthropic.NewClient(apiKey)
		messages := []anthropic.Message{
			anthropic.NewUserTextMessage(*params.Query),
		}

		// Claude API require a max token
		if *params.MaxTokens <= 0 {
			*params.MaxTokens = 1000
		}

		if !*params.Stream {
			resp, err := client.CreateMessages(context.Background(), anthropic.MessagesRequest{
				Model:     params.SelectedModel.Name,
				Messages:  messages,
				MaxTokens: *params.MaxTokens,
			})
			if err != nil {
				var e *anthropic.APIError
				if errors.As(err, &e) {
					fmt.Printf("Messages error, type: %s, message: %s", e.Type, e.Message)
				} else {
					fmt.Printf("Messages error: %v\n", err)
				}

				return
			}

			content = resp.Content[0].Text
		} else {
			resp, err := client.CreateMessagesStream(context.Background(), anthropic.MessagesStreamRequest{
				MessagesRequest: anthropic.MessagesRequest{
					Model:     params.SelectedModel.Name,
					Messages:  messages,
					MaxTokens: *params.MaxTokens,
				},
				OnContentBlockDelta: func(data anthropic.MessagesEventContentBlockDeltaData) {
					fmt.Printf(data.Delta.Text)
				},
			})
			if err != nil {
				var e *anthropic.APIError
				if errors.As(err, &e) {
					fmt.Printf("Messages stream error, type: %s, message: %s", e.Type, e.Message)
				} else {
					fmt.Printf("Messages stream error: %v\n", err)
				}

				return
			}

			content = resp.Content[0].Text
		}

		(*sess)[step].Assistant = content
		println(content)
	}

	if params.SelectedModel.Provider == OPENAI_PROVIDER {
		config := openai.DefaultConfig(apiKey)
		if val := os.Getenv("OPENAI_BASEURL"); val != "" {
			config.BaseURL = val
		}

		client := openai.NewClientWithConfig(config)
		messages := &[]openai.ChatCompletionMessage{
			{
				Role:    openai.ChatMessageRoleSystem,
				Content: *params.Prompt,
			},
			{
				Role:    openai.ChatMessageRoleUser,
				Content: *params.Query,
			},
		}

		if !*params.Stream {
			resp, err := client.CreateChatCompletion(context.Background(), openai.ChatCompletionRequest{
				Model:    params.SelectedModel.Name,
				Messages: *messages,
			},
			)
			if err != nil {
				var e *openai.APIError
				if errors.As(err, &e) {
					fmt.Printf("Messages stream error, type: %s, message: %s", e.Type, e.Message)
				} else {
					fmt.Printf("Messages stream error: %v\n", err)
				}

				return
			}

			content = resp.Choices[0].Message.Content
			fmt.Println(content)
		} else {
			stream, err := client.CreateChatCompletionStream(context.Background(), openai.ChatCompletionRequest{
				Model:    params.SelectedModel.Name,
				Messages: *messages,
			},
			)
			if err != nil {
				var e *openai.APIError
				if errors.As(err, &e) {
					fmt.Printf("Messages stream error, type: %s, message: %s", e.Type, e.Message)
				} else {
					fmt.Printf("Messages stream error: %v\n", err)
				}

				return
			}

			defer stream.Close()

			for {
				resp, err := stream.Recv()
				if errors.Is(err, io.EOF) {
					break
				}

				if err != nil {
					log.Fatal(err)
				}

				fmt.Printf(resp.Choices[0].Delta.Content)
				content = content + resp.Choices[0].Delta.Content
			}

			fmt.Println()
		}
	}

	(*sess)[step].Assistant = content
}

func getApiKey(provider Provider) string {
	apiKey := os.Getenv(provider.ApiKeyVariableName)
	if len(apiKey) == 0 {
		apiKey = viper.GetString("api-key")
		if len(apiKey) == 0 {
			log.Fatal("no api-key variable set, nor is api-key in config file")
		}
	}

	return apiKey
}

func processCLI(in string, p *Parameters) bool {
	cmd := strings.Split(in, " ")
	if len(cmd) < 1 {
		return false
	}

	var val string
	if len(cmd) > 1 {
		val = strings.Join(cmd[1:], " ")
	}

	switch cmd[0] {
	case "prompt:":
		*p.Prompt = val
	case "loadprompt:":
		p.Prompt = loadPrompt(val)
	case "maxtokens:":
		v, err := strconv.ParseInt(val, 10, 0)
		if err != nil {
			log.Fatal(err)
		}

		*p.MaxTokens = int(v)
	case "model:":
		*p.SelectedModel = *setModel(val)
	case "showprompt":
		*p.ShowPrompt = !*p.ShowPrompt
	case "showquery":
		*p.ShowQuery = !*p.ShowQuery
	case "dryrun":
		*p.DryRun = !*p.DryRun
	case "price":
		*p.Price = !*p.Price
	case "raw":
		*p.Raw = !*p.Raw
	case "stream":
		*p.Stream = !*p.Stream
	case "show":
		t := reflect.TypeOf(*p)
		v := reflect.ValueOf(*p)

		for i := 0; i < v.NumField(); i++ {
			field := t.Field(i)
			value := v.Field(i)
			fmt.Printf("%s: %v\n", strings.ToLower(field.Name), value.Elem())
		}
	case "help":
		for k, v := range showHelp {
			fmt.Printf("%s: %s\n", k, v)
		}

	case "exit":
		return true

	default:
		*p.Query = strings.Join(cmd, " ")
	}

	return false
}

func lsDir(dirpath string) {
	dir, err := os.Open(dirpath)
	if err != nil {
		log.Fatal(err)
	}
	defer dir.Close()

	files, err := dir.ReadDir(0)
	if err != nil {
		panic(err)
	}

	for _, file := range files {
		b := path.Base(file.Name())
		x := path.Ext(file.Name())
		fmt.Println(b[:len(b)-len(x)])
	}
}

func main() {
	err := readConf()
	if err != nil {
		log.Fatal(err)
	}

	var modelsList []string

	for _, val := range MODELS {
		modelsList = append(modelsList, val.Name)
	}

	modelsListStr := strings.Join(modelsList, ", ")

	params := Parameters{
		Prompt:      flag.String("ip", "", "inline prompt"),
		LoadPrompt:  flag.String("p", "", "path to prompt"),
		EditPrompt:  flag.String("ep", "", "path to prompt"),
		Session:     flag.String("S", "last", "session name"),
		Model:       flag.String("c", modelsList[0], fmt.Sprintf("model name: %s", modelsListStr)),
		ShowPrompt:  flag.Bool("sp", false, "show prompt"),
		LsPrompt:    flag.Bool("lp", false, "list prompts"),
		ShowQuery:   flag.Bool("sq", false, "show query"),
		MaxTokens:   flag.Int("m", 0, "max tokens"),
		DryRun:      flag.Bool("d", false, "dry run"),
		Raw:         flag.Bool("r", false, "raw output"),
		Interactive: flag.Bool("i", false, "interactive session"),
		Stream:      flag.Bool("s", false, "streaming session"),
		Price:       flag.Bool("t", false, "token count and price"),
		LastSession: flag.Bool("l", false, "read last session"),
	}
	sflags := SFlags{
		sls:  flag.Bool("session-list", false, "list sessions"),
		spwd: flag.Bool("session-path", false, "path to sessions files"),
		scat: flag.Bool("session-dump", false, "dump session to stdout"),
		srm:  flag.Bool("session-delete", false, "delete session"),
		smv:  flag.Bool("session-rename", false, "rename session"),
	}

	flag.Parse()
	argv := flag.Args()

	// read previous sessions
	sess := []Session{}
	readSession := false

	if sdir := viper.GetString("sessions"); len(sdir) > 0 {
		sessionsDir = sdir
	}

	if pdir := viper.GetString("prompts"); len(pdir) > 0 {
		promptsDir = pdir
	}

	// List prompts
	if *params.LsPrompt {
		lsDir(promptsDir)
		os.Exit(0)
	}
	// Edit prompt
	if len(*params.EditPrompt) > 0 {
		ed := os.Getenv("EDITOR")
		if len(ed) < 1 {
			log.Fatal("$EDITOR environment variable is needed to edit a prompt")
		}

		cmd := exec.Command(ed, path.Join(promptsDir, *params.EditPrompt))
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		err := cmd.Run()

		if err != nil {
			log.Fatal(err)
		}

		os.Exit(0)
	}

	sessionFile := path.Join(sessionsDir, *params.Session+".json")

	f, err := os.OpenFile(sessionFile, os.O_CREATE|os.O_RDWR, 0o644)
	if err != nil {
		log.Fatalf("can't open %s: %v", sessionFile, err)
	}

	defer f.Close()
	// load session if session is named (-S) or -l and not loaded already
	if *params.Session != "last" || *params.LastSession && len(sess) < 1 {
		stat, err := os.Stat(sessionFile)
		if err == nil && stat.Size() > 0 {
			decoder := json.NewDecoder(f)
			if err := decoder.Decode(&sess); err != nil {
				log.Fatalf("can't decode %s: %v", sessionFile, err)
			}

			readSession = true
		}
	}

	if *sflags.scat {
		for _, s := range sess {
			printOut(*params.Raw, "# prompt\n"+s.System)
			printOut(*params.Raw, "# query\n"+s.User)
			printOut(*params.Raw, "# reply\n"+s.Assistant)
		}

		os.Exit(0)
	}
	// handle other session flags
	if sessionFlags(sflags, sessionFile, argv) {
		os.Exit(0)
	}
	// session is either loaded or we start from scratch
	if err := f.Truncate(0); err != nil {
		log.Fatal(err)
	}

	if _, err = f.Seek(0, 0); err != nil {
		log.Fatal(err)
	}
	// init the encoder for later writing
	encoder := json.NewEncoder(f)

	if len(argv) < 1 && !*params.Interactive {
		log.Fatal("no query given")
	}

	q := strings.Join(argv, " ")

	// Load prompt if any and not already set
	if len(*params.Prompt) == 0 {
		params.Prompt = loadPrompt(*params.LoadPrompt)
	}

	// Read data from stdin if any
	stat, _ := os.Stdin.Stat()
	if (stat.Mode() & os.ModeCharDevice) == 0 {
		scanner := bufio.NewScanner(os.Stdin)

		preq := []string{"```"}
		for scanner.Scan() {
			preq = append(preq, scanner.Text())
		}

		preq = append(preq, "```\n")

		if len(preq) > 0 {
			q = strings.Join(preq, " ") + q
		}
	}

	params.Query = &q

	var reader *bufio.Reader
	if *params.Interactive {
		reader = bufio.NewReader(os.Stdin)
	}

	params.SelectedModel = setModel(*params.Model)

	for i := 0; ; i++ {
		// only show the price of this query
		if *params.Price {
			getTokenPrices(
				*params.Raw,
				*params.MaxTokens,
				[]string{*params.Prompt, *params.Query},
			)

			*params.Query = ""

			continue
		}

		system, user := "", ""

		if len(sess) > i { // a session has been loaded, feed GPT with it
			system = sess[i].System
			user = sess[i].User
		} else { // current query
			// interactive mode and no query passed
			if *params.Interactive && len(argv) < 1 {
				fmt.Printf("%s> ", *params.Model)

				in, err := reader.ReadString('\n')
				if err != nil {
					log.Fatal(err)
				}

				in = strings.TrimSuffix(in, "\n")

				if processCLI(in, &params) {
					break
				}
				// we want at least a query
				if len(*params.Query) < 1 {
					continue
				}
			}
			// empty args for next query
			argv = []string{}

			system = *params.Prompt
			user = *params.Query
			readSession = false // all previous posts were fed
		}

		if *params.ShowPrompt {
			printOut(*params.Raw, "# prompt\n"+system)
		}

		if *params.ShowQuery {
			printOut(*params.Raw, "# query\n"+user)
		}
		// a session must be loaded, next
		if readSession {
			continue
		}

		if *params.DryRun {
			*params.Query = "" // reset query to detect interactive mode

			continue
		}

		GenerateCompletion(params, &sess, i)

		if !*params.Interactive {
			break
		}

		*params.Query = "" // init query for CLI mode
	} // for

	if err = encoder.Encode(sess); err != nil {
		log.Fatal(err)
	}
}
