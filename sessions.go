package main

import (
	"fmt"
	"log"
	"os"
	"path"
)

type (
	// Session recording
	Session struct {
		System    string `json:"system"`
		User      string `json:"user"`
		Assistant string `json:"assistant"`
	}

	// Session flags
	SFlags struct {
		sls  *bool
		spwd *bool
		scat *bool
		srm  *bool
		smv  *bool
	}
)

var sessionsDir string = "sessions"

func sessionFlags(sflags SFlags, sfile string, argv []string) bool {
	if *sflags.sls {
		lsDir(sessionsDir)

		return true
	}

	if *sflags.spwd {
		fmt.Println(sessionsDir)

		return true
	}

	if *sflags.srm {
		err := os.Remove(sfile)
		if err != nil {
			log.Fatal(err)
		}

		return true
	}

	if *sflags.smv {
		if len(argv) < 1 {
			log.Fatal("no destination name given")
		}

		err := os.Rename(sfile, path.Join(sessionsDir, argv[0]+".json"))
		if err != nil {
			log.Fatal(err)
		}

		return true
	}

	return false
}
